package com.mind.messagingrabbitmq;

import org.springframework.stereotype.Component;

@Component
public class Consumer {

    public void consumeMessage(String message) {
        System.out.println("Consume message:" + message);
    }

}
